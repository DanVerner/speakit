package com.fivetin.speakit;

import org.junit.Test;

import static org.junit.Assert.*;

public class LoginActivityTest {
    private LoginActivity mActivity;

    @Test
    public void editText_isEmpty(){
        mActivity = new LoginActivity();

        boolean result = mActivity.isLoginFieldEmpty("");

        assertEquals(true, result);
    }

    @Test
    public void login_isValid(){
        mActivity = new LoginActivity();

        boolean result = mActivity.isLoginValid("Arthur");

        assertEquals(true, result);
    }
}