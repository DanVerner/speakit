package com.fivetin.speakit;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.fivetin.speakit.adapters.MessageListAdapter;
import com.fivetin.speakit.helpers.Utils;
import com.fivetin.speakit.helpers.WsConfig;
import com.fivetin.speakit.models.Message;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final String TAG_SELF = "self";
    private static final String TAG_NEW = "new";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_EXIT = "exit";

    private Context mContext;

    private Button mButtonSend;
    private EditText mTextInput;

    private OkHttpClient mClient;

    private ListView mListViewChat;
    private MessageListAdapter mAdapter;
    private List<Message> mListMessages;

    private Utils mUtils;

    private String mName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = MainActivity.this;
        mClient = new OkHttpClient();

        mButtonSend = findViewById(R.id.button_send);
        mTextInput = findViewById(R.id.edit_msg);
        mListViewChat = findViewById(R.id.list_view_messages);

        mUtils = new Utils(mContext);

        Intent intent = getIntent();
        mName = intent.getStringExtra("login");

        mListMessages = new ArrayList<>();

        mAdapter = new MessageListAdapter(mContext, mListMessages);
        mListViewChat.setAdapter(mAdapter);

        sendMessage(mButtonSend);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mClient.dispatcher().executorService().shutdown();
    }

    private void sendMessage(Button buttonSend) {
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message m = new Message("Me", mTextInput.getText().toString(), true);
                appendMessage(m);
                start(mTextInput.getText().toString());
                mTextInput.setText("");
            }
        });
    }

    private void start(final String message) {
        Request request = new Request.Builder().url(WsConfig.URL_WEBSOCKET).build();
        WebSocket ws = mClient.newWebSocket(request, new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                Log.i(TAG, "onOpen: response" + response);
                webSocket.send(message);
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                Message m = new Message("Server", text, false);
                appendMessage(m);
                output("Receiving : " + text);
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {
                webSocket.close(1000, null);
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t,Response response) {
                output("Error : " + t.getMessage());
            }
        });
    }

    private void appendMessage(final Message m) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mListMessages.add(m);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void output(final String txt) {
        Log.i(TAG, "output: " + txt);
    }
}
