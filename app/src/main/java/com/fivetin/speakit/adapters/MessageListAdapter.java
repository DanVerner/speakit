package com.fivetin.speakit.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fivetin.speakit.R;
import com.fivetin.speakit.models.Message;

import java.util.List;

public class MessageListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Message> mMessagesItems;

    public MessageListAdapter(Context context, List<Message> messagesItems) {
        mContext = context;
        mMessagesItems = messagesItems;
    }


    @Override
    public int getCount() {
        return mMessagesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mMessagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Message msg = mMessagesItems.get(position);

        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(mMessagesItems.get(position).isSelf()){
            view = mInflater.inflate(R.layout.list_item_message_right, null);
        } else {
            view = mInflater.inflate(R.layout.list_item_message_left, null);
        }

        TextView sender = view.findViewById(R.id.label_msg_from);
        TextView message = view.findViewById(R.id.text_msg);

        sender.setText(msg.getSender());
        message.setText(msg.getMessage());

        return view;
    }
}
