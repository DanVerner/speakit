package com.fivetin.speakit.models;

public class Message {

    private String mSender;
    private String mMessage;

    private boolean mIsSelf;

    public Message() {
    }

    public Message(String sender, String message, boolean isSelf) {
        mSender = sender;
        mMessage = message;
        mIsSelf = isSelf;
    }

    public String getSender() {
        return mSender;
    }

    public void setSender(String sender) {
        mSender = sender;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public boolean isSelf() {
        return mIsSelf;
    }

    public void setSelf(boolean self) {
        mIsSelf = self;
    }
}
