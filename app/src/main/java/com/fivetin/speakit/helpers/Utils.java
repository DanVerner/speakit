package com.fivetin.speakit.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Utils {
    private static final String TAG = "Utils";

    private Context mContext;
    private SharedPreferences mSharedPreferences;

    private static final String KEY_SHARED_PREF = "ANDROID_WEB_CHAT";
    private static final String KEY_SESSION_ID = "sessionId";
    private static final String FLAG_MESSAGE = "message";
    private static final int KEY_MODE_PRIVATE = 0;

    public Utils(Context context) {
        mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(KEY_SHARED_PREF, KEY_MODE_PRIVATE);
    }

    public void storeSessionId(String sessionId){
        Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_SESSION_ID, sessionId);
        editor.apply();
    }

    public String getSessionId(){
        return mSharedPreferences.getString(KEY_SESSION_ID, null);
    }

    public String getSendMessageJSON(String message){
        String json = null;

        try{
            JSONObject jObj = new JSONObject();
            jObj.put("flag", FLAG_MESSAGE);
            jObj.put("sessionId", getSessionId());
            jObj.put("message", message);

            json = jObj.toString();
        } catch (JSONException e){
            Log.e(TAG, "getSendMessageJSON: ", e);
        }

        return json;
    }
}
