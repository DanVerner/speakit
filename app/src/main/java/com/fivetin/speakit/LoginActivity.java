package com.fivetin.speakit;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity{

    private EditText mEditLogin;
    private TextView mTextError;
    private Button mButtonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEditLogin = findViewById(R.id.editText_login);
        mTextError = findViewById(R.id.textView_error);
        mButtonLogin = findViewById(R.id.button_login);

        deleteSpaces();
        loginUser();
    }

    private void deleteSpaces() {
        mEditLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String login = editable.toString().replaceAll(" ", "");
                if(!editable.toString().equals(login)){
                    mEditLogin.setText(login);
                    mEditLogin.setSelection(login.length());
                }
            }
        });
    }

    private void loginUser() {
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = mEditLogin.getText().toString();

                if(!isLoginFieldEmpty(login) && isLoginValid(login))
                    openChatActivity(login);
                else {
                    if (isLoginFieldEmpty(login))
                        mTextError.setText(R.string.login_text_error_empty);
                    else
                        mTextError.setText(R.string.login_text_error_length);
                    mTextError.setVisibility(View.VISIBLE);
                }
            }

            private void openChatActivity(String login) {
                Intent chatActivity = new Intent(LoginActivity.this, MainActivity.class);
                chatActivity.putExtra("login", login);
                startActivity(chatActivity);
            }
        });
    }

    public boolean isLoginFieldEmpty(String login){
        return Objects.equals(login.trim(), "");
    }

    public boolean isLoginValid(String login){
        return login.trim().length() > 3;
    }
}
